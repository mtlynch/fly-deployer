# fly-deployer

## To run

Get a fly.io API token and set:

```bash
export FLY_API_TOKEN="YOUR-FLY-API-TOKEN"
```

On your TinyPilot, run the following commands to generate a WireGuard key pair:

```bash
wg genkey > wg-test.priv && \
  wg pubkey < wg-test.priv > wg-test.pub && \
  cat wg-test.pub
```

Then run:

```bash
TINYPILOT_PUBLIC_KEY='the public key from WireGuard on TinyPilot'
go run main.go -publicKey "${TINYPILOT_PUBLIC_KEY}"
```
