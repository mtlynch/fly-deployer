package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/superfly/flyctl/api"

	"github.com/tiny-pilot/fly-deployer/random"
)

func createAppWithNetwork(client *api.Client, name string, orgId string, preferredRegionCode *string, network *string) (*api.App, error) {
	query := `
		mutation($input: CreateAppInput!) {
			createApp(input: $input) {
				app {
					id
					name
					organization {
						slug
					}
					config {
						definition
					}
					regions {
							name
							code
					}
				}
			}
		}
	`

	req := client.NewRequest(query)

	type createAppInput struct {
		OrganizationID  string  `json:"organizationId"`
		Runtime         string  `json:"runtime"`
		Name            string  `json:"name"`
		PreferredRegion *string `json:"preferredRegion,omitempty"`
		Network         *string `json:"network,omitempty"`
	}
	req.Var("input", createAppInput{
		Name:            name,
		Runtime:         "FIRECRACKER",
		OrganizationID:  orgId,
		PreferredRegion: preferredRegionCode,
		Network:         network,
	})

	data, err := client.Run(req)
	if err != nil {
		return nil, err
	}

	return &data.CreateApp.App, nil
}

func addWireguardPeer(client *api.Client, orgId string, region *string, peerName string, publicKey string) (*api.CreatedWireGuardPeer, error) {
	query := `
		mutation($input: AddWireGuardPeerInput!) {
			addWireGuardPeer(input: $input) {
				pubkey
				endpointip
				peerip
			}
		}
	`

	req := client.NewRequest(query)

	type addWireGuardPeerInput struct {
		OrganizationID string  `json:"organizationId"`
		Region         *string `json:"region,omitempty"`
		Name           string  `json:"name"`
		PublicKey      string  `json:"pubkey"`
	}
	req.Var("input", addWireGuardPeerInput{
		OrganizationID: orgId,
		Region:         region,
		Name:           peerName,
		PublicKey:      publicKey,
	})

	data, err := client.Run(req)
	if err != nil {
		return nil, err
	}

	return &data.AddWireGuardPeer, nil
}

func getServerCfg(client *api.Client, appName string) (*api.AppConfig, error) {
	var definition = map[string]interface{}{
		"experimental": []map[string]interface{}{
			{
				"private_network": true,
			},
		},
		"services": []map[string]interface{}{
			{
				"internal_port": 8085,
				"protocol":      "tcp",
				"ports": []api.PortHandler{
					{
						Handlers: []string{"tls", "http"},
						Port:     443,
					},
				},
				"tcp_checks": []map[string]interface{}{
					{
						"grace_period":  "1s",
						"interval":      "15s",
						"port":          "8085",
						"restart_limit": 6,
						"timeout":       "2s",
					},
				},
			},
		},
	}

	return client.ParseConfig(appName, definition)
}

func main() {
	log.Print("starting fly deployer")

	peerNameFlag := flag.String("peerName", "tinypilot", "Name for tinypilot peer")
	peerPubKeyFlag := flag.String("publicKey", "", "Public key of tinypilot peer")

	flag.Parse()

	peerName := *peerNameFlag
	peerPubKey := *peerPubKeyFlag

	if peerName == "" {
		log.Fatal("-peerName is required")
	}

	if peerPubKey == "" {
		log.Fatal("-publicKey is required")
	}

	token := os.Getenv("FLY_API_TOKEN")
	if token == "" {
		log.Fatal("FLY_API_TOKEN is required")
	}

	api.SetBaseURL("https://api.fly.io")
	client := api.NewClient(token, "0.0.210")
	orgs, err := client.GetOrganizations()
	if err != nil {
		log.Fatalf("error getting organizations: %s", err)
	}
	orgId := orgs[0].ID
	preferredRegion := "iad"
	appName := "tp-proxy-" + random.String(20)
	//network := "dummycustomer"
	log.Printf("creating app %s", appName)
	_, err = createAppWithNetwork(client, appName, orgId, &preferredRegion, nil)
	if err != nil {
		log.Fatalf("error creating app: %v", err)
	}

	// TODO: Is there a way to set env vars aside from secrets?
	log.Printf("Setting secrets")
	secrets := make(map[string]string)
	secrets["EXTERNAL_PORT"] = "8085"
	secrets["TINYPILOT_PROXY_TARGET"] = fmt.Sprintf("%s._peer.internal:443", peerName)
	_, err = client.SetSecrets(appName, secrets)
	if err != nil {
		log.Printf("failed to set secrets: %v", err)
		return
	}

	// TODO: Add certificate to app

	log.Printf("allocating IP address")
	ipAddress, err := client.AllocateIPAddress(appName, "v6")
	if err != nil {
		log.Printf("Failed to allocate IP: %v", err)
		return
	}

	log.Printf("allocated IP: %v", ipAddress.Address)

	// TODO: Update TinyPilot DNS with IP address

	log.Printf("adding WireGuard peer: %s -> %v", peerName, peerPubKey)
	wgPeer, err := addWireguardPeer(client, orgId, &preferredRegion, peerName, peerPubKey)
	if err != nil {
		log.Printf("failed to add wireguard peer: %v", err)
		return
	}
	fmt.Printf("Save this as %s.conf on the tinypilot\n\n", peerName)
	fmt.Printf(`[Interface]
PrivateKey = REPLACE-WITH-YOUR-PRIVATE-KEY
Address = %s
DNS = fdaa:0:1dfc::3

[Peer]
PublicKey = %s
AllowedIPs = fdaa:0:1dfc::/48
Endpoint = %s:51820
PersistentKeepalive = 15

`, wgPeer.Peerip, wgPeer.Pubkey, wgPeer.Endpointip)

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Press ENTER to continue")
	reader.ReadString('\n')

	appCfg, err := getServerCfg(client, appName)
	if err != nil {
		log.Printf("failed to get server config: %v", err)
		return
	}

	log.Printf("deploying image")
	release, _, err := client.DeployImage(api.DeployImageInput{
		AppID:      appName,
		Image:      "us.gcr.io/tinypilot-public-images/fly-haproxy:latest",
		Definition: &appCfg.Definition,
	})
	if err != nil {
		log.Printf("failed to deploy image: %v", err)
		return
	}
	log.Printf("Release v%d created", release.Version)

	log.Print("done")
}
